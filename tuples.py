# Create a tuple
numbers = (1, 2, 3, 4, 5)

# Access elements by index
print(numbers[0]) 

# Iterate over a tuple
for num in numbers:
    print(num)

# Tuple length
length = len(numbers)
print(length)  
